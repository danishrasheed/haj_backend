'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db) {


  return db.createTable('purchase', {
    id: {
      type: 'int',
      unsigned: true,
      notNull: true,
      primaryKey: true,
      autoIncrement: true
    },

    advance: {
      type: 'double'
    },
    dicount: {
      type: 'double'
    },
    total_installment: {
      type: 'double'
    },
    price: {
      type: 'double'
    },

    prod_id: {
      type: 'int',
      unsigned: true,
      notNull: true,
      foreignKey: {
        name: 'prod_id_fk1',
        table: 'product',
        rules: {
          onDelete: 'CASCADE',
          onUpdate: 'RESTRICT'
        },
        mapping: {
          prod_id: 'id'
        }
      }
    },
    disabled: {
      type: 'boolean',
      defaultValue: false
    },

    user_id: {
      type: 'int',
      unsigned: true,
      notNull: true,
      foreignKey: {
        name: 'user_id_fk1',
        table: 'users',
        rules: {
          onDelete: 'CASCADE',
          onUpdate: 'RESTRICT'
        },
        mapping: {
          user_id: 'id'
        }
      }
    },
    market_boy_id: {
      type: 'int',
      unsigned: true,
      foreignKey: {
        name: 'market_boy_id_fk',
        table: 'users',
        rules: {
          onDelete: 'CASCADE',
          onUpdate: 'RESTRICT'
        },
        mapping: {
          market_boy_id: 'id'
        }
      }
    },
    disabled: {
      type: 'boolean',
      defaultValue: false
    }
  }, function (err) {
    console.log(err);
    console.log("Purchase Created");
    return db.createTable('installment', {
      id: {
        type: 'int',
        unsigned: true,
        notNull: true,
        primaryKey: true,
        autoIncrement: true
      },

      payment: {
        type: 'double'
      },
      remaining_balance: {
        type: 'double'
      },
      date: {
        type: 'datetime'
      },

      purchase_id: {
        type: 'int',
        unsigned: true,
        notNull: true,
        foreignKey: {
          name: 'purchase_id_fk',
          table: 'purchase',
          rules: {
            onDelete: 'CASCADE',
            onUpdate: 'RESTRICT'
          },
          mapping: {
            purchase_id: 'id'
          }
        }
      },
      disabled: {
        type: 'boolean',
        defaultValue: false
      }
    }, function (err) {
      console.log(err);
      console.log("Installment Created");

    });
  });





};

exports.down = function(db) {
  return null;
};

exports._meta = {
  "version": 1
};
