'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function (options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function (db) {
  return db.createTable('users', {
    id: {
      type: 'int',
      unsigned: true,
      notNull: true,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: 'string'
    },
    occuption: {
      type: 'string'
    },
    contact: {
      type: 'string'
    },
    address: {
      type: 'string'
    },
    picture: {
      type: 'string'
    },
    cnic: {
      type: 'string'
    },


    dob: {
      type: 'datetime'
    },
    role_id: {
      type: 'int',
      unsigned: true,
      notNull: true,
      foreignKey: {
        name: 'role_id_fk',
        table: 'roles',
        rules: {
          onDelete: 'CASCADE',
          onUpdate: 'RESTRICT'
        },
        mapping: {
          role_id: 'id'
        }
      }
    },
    disabled: {
      type: 'boolean',
      defaultValue: false
    }
  }, function (err) {
    console.log(err);
    console.log("User Created");
     db.createTable('product', {
      id: {
        type: 'int',
        unsigned: true,
        notNull: true,
        primaryKey: true,
        autoIncrement: true
      },
      name: {
        type: 'string'
      },
      price: {
        type: 'double'
      },
      remaining_count: {
        type: 'double'
      },

      cat_id: {
        type: 'int',
        unsigned: true,
        notNull: true,
        foreignKey: {
          name: 'cat_id_fk',
          table: 'category',
          rules: {
            onDelete: 'CASCADE',
            onUpdate: 'RESTRICT'
          },
          mapping: {
            cat_id: 'id'
          }
        }
      },
      disabled: {
        type: 'boolean',
        defaultValue: false
      }
    },
      function (err) {
        console.log(err);
        console.log("Product Created");
         db.createTable('product_stock', {
          id: {
            type: 'int',
            unsigned: true,
            notNull: true,
            primaryKey: true,
            autoIncrement: true
          },

          actual_price: {
            type: 'double'
          },
          sale_price: {
            type: 'double'
          },
          total_piece: {
            type: 'double'
          },

          prod_id: {
            type: 'int',
            unsigned: true,
            notNull: true,
            foreignKey: {
              name: 'prod_id_fk',
              table: 'product',
              rules: {
                onDelete: 'CASCADE',
                onUpdate: 'RESTRICT'
              },
              mapping: {
                prod_id: 'id'
              }
            }
          },
          disabled: {
            type: 'boolean',
            defaultValue: false
          }
        },
          function (err) {
            console.log(err);
            console.log("Product_stock Created");

          });
      });
  });


};

exports.down = function (db) {
  return null;
};

exports._meta = {
  "version": 1
};
