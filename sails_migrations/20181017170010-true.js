'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db) {
  return db.createTable('guarantor', {
    id: {
      type: 'int',
      primaryKey: true,
      autoIncrement: true
    },


    guarantor_id: {
      type: 'int',
      unsigned: true,
      notNull: true,
      foreignKey: {
        name: 'user_id_fk2',
        table: 'users',
        rules: {
          onDelete: 'CASCADE',
          onUpdate: 'RESTRICT'
        },
        mapping: {
          guarantor_id: 'id'
        }
      }
    },
    purchase_id: {
      type: 'int',
      unsigned: true,
      notNull: true,
      foreignKey: {
        name: 'purchase_id_fk3',
        table: 'purchase',
        rules: {
          onDelete: 'CASCADE',
          onUpdate: 'RESTRICT'
        },
        mapping: {
          purchase_id: 'id'
        }
      }
    },
    disabled: {
      type: 'boolean',
      defaultValue: false
    }
  }, function (err) {
    console.log(err);
    console.log("Gurreb Created");
  });
};

exports.down = function(db) {
  return null;
};

exports._meta = {
  "version": 1
};
