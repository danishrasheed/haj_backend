'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function (options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function (db) {
  return db.createTable('roles', {
    id: {
      type: 'int',
      unsigned: true,
      notNull: true,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: 'string'
    },
    disabled : {
      type: 'boolean',
      defaultValue: false
    }
  },
  function(err){
    console.log(err);
 console.log("roles Created");
 return db.createTable('admin', {
    id: {
      type: 'int',
      unsigned: true,
      notNull: true,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: 'string'
    },
 password: {
      type: 'string'
    },
 last_signin: {
      type: 'datetime'
    },
 contact: {
      type: 'string'
    },
    disabled : {
      type: 'boolean',
      defaultValue: false
    }
  },
  function(err){
    console.log(err);
 console.log("Admin Created");
 return db.createTable('category', {
    id: {
      type: 'int',
      unsigned: true,
      notNull: true,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: 'string'
    },
    disabled : {
      type: 'boolean',
      defaultValue: false
    }
  },
  function(err){
    console.log(err);
 console.log("category Created");
  });
  });
  });
};

exports.down = function (db) {
  return null;
};

exports._meta = {
  "version": 1
};
