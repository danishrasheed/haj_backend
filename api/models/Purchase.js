/**
 * Purchase.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
  tableName: 'purchase',
  autoCreatedAt: false,
  autoUpdatedAt: false,

  attributes: {
    id: {
      type: 'int',
      primaryKey: true,
      autoIncrement: true
    },
    advance: {
      type: 'int'
    },
    dicount: {
      type: 'int'
    },
    total_installment: {
      type: 'int'
    },
    price: {
      type: 'int'
    },
    prod_id: {
      model: 'Product',
      type: 'int',
      required: true
    },
    market_boy_id: {
      model:'Users',
      type:'int',
    },
    disabled: {
      type: 'boolean',
      default: false
    }
  }
};
