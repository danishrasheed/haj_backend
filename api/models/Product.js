/**
 * Product.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
  tableName: 'product',
  autoCreatedAt: false,
  autoUpdatedAt: false,

  attributes: {
    id: {
      type: 'int',
      unsigned: true,
      notNull: true,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: 'string'
    },
    price: {
      type: 'int'
    },
    remaining_count: {
      type: 'int'
    },

    cat_id: {
      model: 'Category',
      type: 'int',
      required: true
      },
      disabled: {
        type: 'boolean',
        default: false
      }
    }
  };

