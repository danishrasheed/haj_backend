/**
 * Product_stock.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
  tableName: 'product_stock',
  autoCreatedAt: false,
  autoUpdatedAt: false,

  attributes: {
    id: {
      type: 'int',
      primaryKey: true,
      autoIncrement: true
    },
    actual_price: {
      type: 'Number'
    },
    sale_price: {
      type: 'Number'
    },
    total_piece: {
      type: 'Number'
    },
    prod_id: {
      model: 'Product',
      type: 'int',
      required: true
    },
    disabled: {
        type: 'boolean',
        default: false
    }
  }
};

