/**
 * Admin.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  tableName: 'admin',
  autoCreatedAt: false,
  autoUpdatedAt: false,
  attributes: {
    id: {
      type: 'int',
      autoIncrement: true,
      primaryKey: true
    },
    name: {
      type: 'string'
    },
    //rename
    password: {
      type: 'string'
    },
    last_signin: {
      type: 'datetime'
    },
    contact: {
      type: 'string'
    },
    disabled: {
      type: 'boolean',
      default: false
    }
  }
};

