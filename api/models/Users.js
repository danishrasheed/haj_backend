/**
 * Users.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  tableName: 'users',
  autoCreatedAt: false,
  autoUpdatedAt: false,
  attributes: {
    id: {
      type: 'int',
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: 'string'
    },
    occuption: {
      type: 'string'
    },
    contact: {
      type: 'string'
    },
    address: {
      type: 'string'
    },
    picture: {
      type: 'string'
    },
    cnic: {
      type: 'string'
    },
    dob: {
      type: 'datetime'
    },
    role_id: {
      model: 'Roles',
      type: 'int',
      required: true
    },
    disabled: {
      type: 'boolean',
      default: false
    }
  }
};

