/**
 * Installment.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  tableName: 'installment',
  autoCreatedAt: false,
  autoUpdatedAt: false,
  attributes: {
    id: {
      type: 'int',
      primaryKey: true,
      autoIncrement: true
    },

    payment: {
      type: 'double'
    },
    remaining_balance: {
      type: 'double'
    },
    date: {
      type: 'datetime'
    },

    purchase_id: {
      model: 'Purchase',
      type: 'int',
      required: true
    },
    disabled: {
      type: 'boolean',
      default: false
    }

  }
};

