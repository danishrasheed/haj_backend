/**
 * Product_stockController
 *
 * @description :: Server-side logic for managing product_stocks
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	createStock:(req,res,next)=>{
        var params = req.params.all;
        console.log(params);
        Product_stock.create({
            price:params.price,
            actual_price:params.actual_price,
            sale_price:params.sale_price,
            total_piece:params.total_piece,
            prod_id:params.prod_id,
            disabled:false,
        })
        .exec((err,data)=>{
            if(data){
                Product.findOne({id:params.prod_id})
                .exec((err,pro)=>{
                    if(pro) {
                        pro.remaining_count = pro.remaining_count + params.total_piece;
                        pro.save();
                        res.ok();
                    } else {
                        res.serverError();   
                    }
                })
            } else {
                res.serverError();
            }

        })
        
    }
};

