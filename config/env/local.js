module.exports = {
    datastores: {
        default: {
            adapter: 'sails-mysql',
            host: 'localhost',
            port: 3306,
            user: 'root',
            password: '123',
            database: 'haj_db',
                    
        }
    },
    models: {
        migrate: 'safe'
    },
    blueprints: {
        shortcuts: true,
    },
    port: 3003
}